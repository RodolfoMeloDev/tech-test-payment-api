﻿using App.Data.Implementations;
using App.Domain.Dtos.Seller;
using App.Domain.Enums;
using App.Services.Services;
using Xunit;

namespace App.Services.Test
{
    public class SellerServiceTests : DBTest
    {
        private SellerImplementation _repository;

        public SellerServiceTests()
        {
            _repository = new SellerImplementation(_context);
        }

        [Fact]
        public async void SellerService_SellerCreated_Success()
        {
            // Arrange
            var seller = new SellerCreateDto { Name = "Test", Cpf = "901.533.030-18", Email = "teste@teste.com", Phone = null };
            var service = new SellerService(_repository, _mapper);

            // Act
            var result = await service.Insert(seller);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Id > 0);
            Assert.True(result.DateCreated != DateTime.MinValue);
            Assert.Equal(seller.Name.ToUpper(), result.Name);
            Assert.Equal(seller.Email.ToUpper(), result.Email);
            Assert.Equal(seller.Phone, result.Phone);
        }

        [Fact]
        public async Task SellerService_SellerCreated_Fail_AmbigusCPF()
        {
            // Arrange
            var seller1 = new SellerCreateDto { Name = "Test", Cpf = "097.112.710-79", Email = "teste@teste.com", Phone = null };
            var seller2 = new SellerCreateDto { Name = "Test", Cpf = "097.112.710-79", Email = "teste@teste.com", Phone = null };

            var service = new SellerService(_repository, _mapper);

            await service.Insert(seller1);

            // Act
            var ex = await Assert.ThrowsAsync<Exception>(() => service.Insert(seller2));

            // Assert
            Assert.Contains($"Já existe um vendedor cadastro com o CPF: {seller2.Cpf}", ex.Message);

        }

        [Fact]
        public async Task SellerService_SellerCreated_Fail_InvalidCPF()
        {
            // Arrange
            var seller1 = new SellerCreateDto { Name = "Test", Cpf = "097.112.710-70", Email = "teste@teste.com", Phone = null };
            var seller2 = new SellerCreateDto { Name = "Test", Cpf = "097.112.710-09", Email = "teste@teste.com", Phone = null };
            var seller3 = new SellerCreateDto { Name = "Test", Cpf = "111.111.111-11", Email = "teste@teste.com", Phone = null };

            var service = new SellerService(_repository, _mapper);

            // Act
            var exDigit1 = await Assert.ThrowsAsync<Exception>(() => service.Insert(seller1));
            var exDigit2 = await Assert.ThrowsAsync<Exception>(() => service.Insert(seller2));
            var exDigit3 = await Assert.ThrowsAsync<Exception>(() => service.Insert(seller3));

            // Assert
            Assert.Contains($"O CPF: {seller1.Cpf} não é válido!", exDigit1.Message);
            Assert.Contains($"O CPF: {seller2.Cpf} não é válido!", exDigit2.Message);
            Assert.Contains($"O CPF: {seller3.Cpf} não é válido!", exDigit3.Message);

        }

        [Fact]
        public async Task SellerService_SellerUpdate_Success()
        {
            // Arrange
            var seller = new SellerUpdateDto { Id = 1, Name = "Test", Cpf = "901.533.030-18", Email = "teste@teste.com", Status = StatusRegister.INACTIVE };
            var service = new SellerService(_repository, _mapper);

            // Act
            var result = await service.Update(seller);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(seller.Id, result.Id);
            Assert.Equal(seller.Name.ToUpper(), result.Name);
            Assert.Equal(seller.Email.ToUpper(), result.Email);
            Assert.Null(result.Phone);
            Assert.Equal(StatusRegister.INACTIVE, result.Status);
            Assert.NotNull(result.DateUpdated);
            Assert.True(result.DateUpdated > result.DateCreated);
        }

        [Fact]
        public async Task SellerService_SellerUpdate_Fail_Different_Id_By_CPF()
        {
            // Arrange
            var seller = new SellerUpdateDto { Id = 2, Name = "Test", Cpf = "901.533.030-18", Email = "teste@teste.com", Status = StatusRegister.INACTIVE };
            var service = new SellerService(_repository, _mapper);

            // Act
            var ex = await Assert.ThrowsAsync<Exception>(() => service.Update(seller));

            // Assert
            Assert.Equal("O código do vendedor informado é inválido para o CPF", ex.Message);
        }

        [Fact]
        public async Task SellerService_SellerUpdate_Fail_Invalid_CPF()
        {
            // Arrange
            var seller = new SellerUpdateDto { Id = 1, Name = "Test", Cpf = "111.111.111-11", Email = "teste@teste.com", Status = StatusRegister.INACTIVE };
            var service = new SellerService(_repository, _mapper);

            // Act
            var ex = await Assert.ThrowsAsync<Exception>(() => service.Update(seller));

            // Assert
            Assert.Equal($"O CPF: {seller.Cpf} não é válido!", ex.Message);
        }


        [Fact]
        public async void SellerService_SellerGetById()
        {
            // Arrange
            var sellerId = 1;
            var service = new SellerService(_repository, _mapper);

            // Act
            var resultGet = await service.GetById(sellerId);

            // Assert
            Assert.NotNull(resultGet);
            Assert.Equal(sellerId, resultGet.Id);
            Assert.True(resultGet.DateCreated > DateTime.MinValue);
        }

        [Fact]
        public async void SellerService_SellerGetByCPF()
        {
            // Arrange
            var sellerCPF = "901.533.030-18";
            var service = new SellerService(_repository, _mapper);

            // Act
            var resultGet = await service.GetByCPF(sellerCPF);

            // Assert
            Assert.NotNull(resultGet);
            Assert.Equal(sellerCPF, resultGet.Cpf);
            Assert.True(resultGet.DateCreated > DateTime.MinValue);
        }

        [Fact]
        public async void SellerService_SellersGetAll()
        {
            // Arrange
            var service = new SellerService(_repository, _mapper);

            // Act
            var result = await service.GetAll();

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Count() > 0);
        }

        [Fact]
        public async void SellerService_SellerDeleteById_Success()
        {
            // Arrange
            var seller = new SellerCreateDto { Name = "Test", Cpf = "334.181.800-60", Email = "teste@teste.com", Phone = null };
            var service = new SellerService(_repository, _mapper);

            // Act
            var result = await service.Insert(seller);
            var resultDelete = await service.Delete(result.Id);

            // Assert
            Assert.True(resultDelete);
        }

        [Fact]
        public async void SellerService_SellersDeleteById_Fail()
        {
            // Arrange
            var service = new SellerService(_repository, _mapper);

            // Act
            var resultDelete = await service.Delete(9999);

            // Assert
            Assert.False(resultDelete);
        }
    }
}
