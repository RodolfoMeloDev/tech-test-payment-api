﻿using App.Data.Implementations;
using App.Domain.Dtos.Sale;
using App.Domain.Dtos.SaleItems;
using App.Domain.Dtos.Seller;
using App.Domain.Enums;
using App.Domain.Interfaces.Repositories;
using App.Domain.Interfaces.Services;
using App.Services.Services;
using AutoMapper;
using Castle.DynamicProxy.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace App.Services.Test
{
    public class SaleServiceTests : DBTest
    {
        private SaleImplementation _saleRepository;
        private SaleItemsImplementation _saleItemsRepository;
        private SellerImplementation _sellerRepository;

        private SaleService _saleService;
        private SellerService _sellerService;
        private SaleItemsService _saleItemsService;

        public SaleServiceTests()
        {
            _saleRepository = new SaleImplementation(_context);
            _saleItemsRepository = new SaleItemsImplementation(_context);
            _sellerRepository = new SellerImplementation(_context);

            _sellerService = new SellerService(_sellerRepository, _mapper);
            _saleItemsService = new SaleItemsService(_saleItemsRepository, _mapper);
            _saleService = new SaleService(_saleRepository, _sellerService, _saleItemsService, _mapper);
        }

        [Fact]
        public async void SaleService_SaleCreated_Insert_Success()
        {
            // Arrange
            List<SaleItemsCreateListDto> listItems = new List<SaleItemsCreateListDto>();
            decimal TotalSale = 0;
            for (int i = 0; i < 4; i++)
            {
                var dto = new SaleItemsCreateListDto
                {
                    ProductId = i + 1,
                    UnitPrice = Convert.ToDecimal(1.10) * (i + 1),
                    Amount = 2
                };

                TotalSale += (Convert.ToDecimal(1.10) * (i + 1) * 2);

                listItems.Add(dto);
            }
            var seller = new SellerCreateDto { Name = "Test", Cpf = "631.109.020-10", Email = "teste@teste.com", Phone = null };
            var sale = new SaleCreateDto { SellerId = 1, DatePurchase= DateTime.Now, saleItems = listItems };


            // Act
            var resultSeller = await _sellerService.Insert(seller);
            var resultSale = await _saleService.Insert(sale);

            // Assert
            Assert.NotNull(resultSale);
            Assert.True(resultSale.Id > 0);
            Assert.True(resultSale.DateCreated != DateTime.MinValue);
            Assert.Equal(resultSale.DatePurchase, sale.DatePurchase);
            Assert.True(resultSale.SaleItems.Count() > 0);
            Assert.Equal(resultSale.TotalSale, TotalSale);
        }

        [Fact]
        public async void SaleService_SaleCreated_Insert_Fails()
        {
            // Arrange            
            var sale1 = new SaleCreateDto { SellerId = 1, DatePurchase = DateTime.Today.AddDays(-1) , saleItems = new List<SaleItemsCreateListDto>() };
            var sale2 = new SaleCreateDto { SellerId = 1, DatePurchase = DateTime.Today, saleItems = new List<SaleItemsCreateListDto>() };

            // Act
            var exDataPurchase = await Assert.ThrowsAsync<Exception>(() => _saleService.Insert(sale1));
            var exSaleItems = await Assert.ThrowsAsync<Exception>(() => _saleService.Insert(sale2));

            // Assert
            Assert.Contains($"A data da compra não pode ser inferior a data atual.", exDataPurchase.Message);
            Assert.Contains($"A compra deve possuir ao menos 1 produto", exSaleItems.Message);
        }

        [Fact]
        public async void SaleService_SaleCreated_InsertBySellerByCPF_Success()
        {
            // Arrange

            List<SaleItemsCreateListDto> listItems = new List<SaleItemsCreateListDto>();
            decimal TotalSale = 0;
            for (int i = 0; i < 4; i++)
            {
                var dto = new SaleItemsCreateListDto
                {
                    ProductId = i + 1,
                    UnitPrice = Convert.ToDecimal(1.10) * (i + 1),
                    Amount = 2
                };

                TotalSale += (Convert.ToDecimal(1.10) * (i + 1) * 2);

                listItems.Add(dto);
            }
            var seller = new SellerCreateDto { Name = "Test", Cpf = "408.303.470-03", Email = "teste@teste.com", Phone = null };
            var sale = new SaleCreateByCPFDto { Cpf = "408.303.470-03", DatePurchase = DateTime.Now, saleItems = listItems };


            // Act
            var resultSeller = await _sellerService.Insert(seller);
            var resultSale = await _saleService.InsertBySellerCPF(sale);

            // Assert
            Assert.NotNull(resultSale);
            Assert.True(resultSale.Id > 0);
            Assert.True(resultSale.DateCreated != DateTime.MinValue);
            Assert.Equal(resultSale.DatePurchase, sale.DatePurchase);
            Assert.True(resultSale.SaleItems.Count() > 0);
            Assert.Equal(resultSale.TotalSale, TotalSale);
        }

        [Fact]
        public async void SaleService_SaleCreated_InsertBySellerByCPF_Fails()
        {
            // Arrange            
            var sale1 = new SaleCreateByCPFDto { Cpf = "102.632.290-17", DatePurchase = DateTime.Today, saleItems = new List<SaleItemsCreateListDto>() };
            var sale2 = new SaleCreateByCPFDto { Cpf = "408.303.470-03", DatePurchase = DateTime.Today.AddDays(-1), saleItems = new List<SaleItemsCreateListDto>() };
            var sale3 = new SaleCreateByCPFDto { Cpf = "408.303.470-03", DatePurchase = DateTime.Today, saleItems = new List<SaleItemsCreateListDto>() };

            // Act
            var exCPF = await Assert.ThrowsAsync<Exception>(() => _saleService.InsertBySellerCPF(sale1));
            var exDataPurchase = await Assert.ThrowsAsync<Exception>(() => _saleService.InsertBySellerCPF(sale2));
            var exSaleItems = await Assert.ThrowsAsync<Exception>(() => _saleService.InsertBySellerCPF(sale3));

            // Assert
            Assert.Contains($"Não foi encontrado o vendedor com o CPF: {sale1.Cpf}", exCPF.Message);
            Assert.Contains($"A data da compra não pode ser inferior a data atual.", exDataPurchase.Message);
            Assert.Contains($"A compra deve possuir ao menos 1 produto", exSaleItems.Message);
        }

        [Fact]
        public async void SaleService_SaleCreated_InsertByNewSeller_Success()
        {
            // Arrange
            List<SaleItemsCreateListDto> listItems = new List<SaleItemsCreateListDto>();
            decimal TotalSale = 0;
            for (int i = 0; i < 4; i++)
            {
                var dto = new SaleItemsCreateListDto
                {
                    ProductId = i + 1,
                    UnitPrice = Convert.ToDecimal(1.10) * (i + 1),
                    Amount = 2
                };

                TotalSale += (Convert.ToDecimal(1.10) * (i + 1) * 2);

                listItems.Add(dto);
            }
            var sale = new SaleCreateNewSellerDto { Name = "Teste new Seller", Cpf = "238.354.410-86", Email = "newseller@teste.com", Phone = null, DatePurchase = DateTime.Now, saleItems = listItems };

            // Act
            var resultSale = await _saleService.InsertByNewSeller(sale);

            // Assert
            Assert.NotNull(resultSale);
            Assert.True(resultSale.Id > 0);
            Assert.True(resultSale.DateCreated != DateTime.MinValue);
            Assert.Equal(resultSale.DatePurchase, sale.DatePurchase);
            Assert.True(resultSale.SaleItems.Count() > 0);
            Assert.Equal(resultSale.TotalSale, TotalSale);
        }

        [Fact]
        public async void SaleService_SaleCreated_InsertByNewSeller_Fails()
        {
            // Arrange            
            var sale1 = new SaleCreateNewSellerDto { Name = "Teste new Seller", Cpf = "912.559.230-08", Email = "newseller@teste.com", Phone = null, DatePurchase = DateTime.Today.AddDays(-1), saleItems = new List<SaleItemsCreateListDto>() };
            var sale2 = new SaleCreateNewSellerDto { Name = "Teste new Seller", Cpf = "680.017.770-90", Email = "newseller@teste.com", Phone = null, DatePurchase = DateTime.Now, saleItems = new List<SaleItemsCreateListDto>() };

            // Act
            var exDataPurchase = await Assert.ThrowsAsync<Exception>(() => _saleService.InsertByNewSeller(sale1));
            var exSaleItems = await Assert.ThrowsAsync<Exception>(() => _saleService.InsertByNewSeller(sale2));

            // Assert
            Assert.Contains($"A data da compra não pode ser inferior a data atual.", exDataPurchase.Message);
            Assert.Contains($"A compra deve possuir ao menos 1 produto", exSaleItems.Message);
        }

        [Fact]
        public async void SaleService_SaleUpdates_Success()
        {
            // Arrange
            List<SaleItemsCreateListDto> listItems = new List<SaleItemsCreateListDto>() { new SaleItemsCreateListDto { ProductId = 1, Amount = 5, UnitPrice = 1 } };
            var saleCreate = new SaleCreateDto { SellerId = 1, DatePurchase= DateTime.Now, saleItems = listItems };
            var resultCreate = await _saleService.Insert(saleCreate);            
            var saleUpdate = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.PAYMENT_ACCEPT };

            // Act
            var result = await _saleService.Update(saleUpdate);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Id == saleUpdate.Id);
            Assert.True(result.StatusSale == saleUpdate.StatusSale);
            Assert.True(result.DateCreated > DateTime.MinValue);
            Assert.NotNull(result.DateUpdated);
            Assert.True(result.DateUpdated > result.DateCreated);

            // Arrange
            var saleUpdate2 = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.SEND_TO_CARRIER };

            // Act
            var result2 = await _saleService.Update(saleUpdate2);

            // Assert
            Assert.NotNull(result2);
            Assert.True(result2.Id == saleUpdate.Id);
            Assert.True(result2.StatusSale == saleUpdate2.StatusSale);
            Assert.True(result2.DateCreated > DateTime.MinValue);
            Assert.NotNull(result2.DateUpdated);
            Assert.True(result2.DateUpdated > result2.DateCreated);
            Assert.True(result2.DateUpdated > result2.DateCreated);
            Assert.True(result2.DateUpdated > result.DateUpdated);

            // Arrange
            var saleUpdate3 = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.DELIVERED };

            // Act
            var result3 = await _saleService.Update(saleUpdate3);

            // Assert
            Assert.NotNull(result3);
            Assert.True(result3.Id == saleUpdate.Id);
            Assert.True(result3.StatusSale == saleUpdate3.StatusSale);
            Assert.True(result3.DateCreated > DateTime.MinValue);
            Assert.NotNull(result3.DateUpdated);
            Assert.True(result3.DateUpdated > result3.DateCreated);
            Assert.True(result3.DateUpdated > result3.DateCreated);
            Assert.True(result3.DateUpdated > result2.DateUpdated);
        }

        [Fact]
        public async void SaleService_SaleUpdates_Canceled_Success()
        {
            // Arrange
            List<SaleItemsCreateListDto> listItems = new List<SaleItemsCreateListDto>() { new SaleItemsCreateListDto { ProductId = 1, Amount = 5, UnitPrice = 1 } };
            var saleCreate = new SaleCreateDto { SellerId = 1, DatePurchase = DateTime.Now, saleItems = listItems };
            var resultCreate = await _saleService.Insert(saleCreate);
            var saleUpdate = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.CANCELED };

            // Act
            var result = await _saleService.Update(saleUpdate);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Id == saleUpdate.Id);
            Assert.True(result.StatusSale == saleUpdate.StatusSale);
            Assert.True(result.DateCreated > DateTime.MinValue);
            Assert.NotNull(result.DateUpdated);
            Assert.True(result.DateUpdated > result.DateCreated);            
        }

        [Fact]
        public async void SaleService_SaleUpdates_Fails_SameStatus()
        {
            // Arrange
            List<SaleItemsCreateListDto> listItems = new List<SaleItemsCreateListDto>() { new SaleItemsCreateListDto { ProductId = 1, Amount = 5, UnitPrice = 1 } };
            var saleCreate = new SaleCreateDto { SellerId = 1, DatePurchase = DateTime.Now, saleItems = listItems };
            var resultCreate = await _saleService.Insert(saleCreate);
            var saleUpdate = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.AWAITING_PAYMENT };

            // Act
            var ex = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate));

            // Assert
            Assert.Equal("A venda já está no status indicado.", ex.Message);
        }

        [Fact]
        public async void SaleService_SaleUpdates_Fails_AwaitingPaymentStatus()
        {
            // Arrange
            List<SaleItemsCreateListDto> listItems = new List<SaleItemsCreateListDto>() { new SaleItemsCreateListDto { ProductId = 1, Amount = 5, UnitPrice = 1 } };
            var saleCreate = new SaleCreateDto { SellerId = 1, DatePurchase = DateTime.Now, saleItems = listItems };
            var resultCreate = await _saleService.Insert(saleCreate);
            var saleUpdate = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.SEND_TO_CARRIER };
            var saleUpdate1 = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.DELIVERED };

            // Act
            var ex1 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate));
            var ex2 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate1));

            // Assert
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Aguardando pagamento Status Atualização: Enviado para Transportadora", ex1.Message);
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Aguardando pagamento Status Atualização: Entregue", ex2.Message);
        }

        [Fact]
        public async void SaleService_SaleUpdates_Fails_PaymentAcceptStatus()
        {
            // Arrange
            List<SaleItemsCreateListDto> listItems = new List<SaleItemsCreateListDto>() { new SaleItemsCreateListDto { ProductId = 1, Amount = 5, UnitPrice = 1 } };
            var saleCreate = new SaleCreateDto { SellerId = 1, DatePurchase = DateTime.Now, saleItems = listItems };
            var resultCreate = await _saleService.Insert(saleCreate);
            await _saleService.Update(new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.PAYMENT_ACCEPT });

            var saleUpdate = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.AWAITING_PAYMENT };
            var saleUpdate1 = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.DELIVERED };

            // Act
            var ex1 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate));
            var ex2 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate1));

            // Assert
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Pagamento Aprovado Status Atualização: Aguardando pagamento", ex1.Message);
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Pagamento Aprovado Status Atualização: Entregue", ex2.Message);
        }

        [Fact]
        public async void SaleService_SaleUpdates_Fails_SendToCarrierStatus()
        {
            // Arrange
            List<SaleItemsCreateListDto> listItems = new List<SaleItemsCreateListDto>() { new SaleItemsCreateListDto { ProductId = 1, Amount = 5, UnitPrice = 1 } };
            var saleCreate = new SaleCreateDto { SellerId = 1, DatePurchase = DateTime.Now, saleItems = listItems };
            var resultCreate = await _saleService.Insert(saleCreate);
            await _saleService.Update(new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.PAYMENT_ACCEPT });
            await _saleService.Update(new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.SEND_TO_CARRIER });

            var saleUpdate = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.AWAITING_PAYMENT };
            var saleUpdate1 = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.CANCELED };

            // Act
            var ex1 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate));
            var ex2 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate1));

            // Assert
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Enviado para Transportadora Status Atualização: Aguardando pagamento", ex1.Message);
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Enviado para Transportadora Status Atualização: Cancelada", ex2.Message);
        }

        [Fact]
        public async void SaleService_SaleUpdates_Fails_DeliveredStatus()
        {
            // Arrange
            List<SaleItemsCreateListDto> listItems = new List<SaleItemsCreateListDto>() { new SaleItemsCreateListDto { ProductId = 1, Amount = 5, UnitPrice = 1 } };
            var saleCreate = new SaleCreateDto { SellerId = 1, DatePurchase = DateTime.Now, saleItems = listItems };
            var resultCreate = await _saleService.Insert(saleCreate);
            await _saleService.Update(new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.PAYMENT_ACCEPT });
            await _saleService.Update(new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.SEND_TO_CARRIER });
            await _saleService.Update(new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.DELIVERED });

            var saleUpdate = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.AWAITING_PAYMENT };
            var saleUpdate1 = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.PAYMENT_ACCEPT };
            var saleUpdate2 = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.SEND_TO_CARRIER };
            var saleUpdate3 = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.CANCELED };

            // Act
            var ex1 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate));
            var ex2 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate1));
            var ex3 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate2));
            var ex4 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate3));

            // Assert
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Entregue Status Atualização: Aguardando pagamento", ex1.Message);
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Entregue Status Atualização: Pagamento Aprovado", ex2.Message);
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Entregue Status Atualização: Enviado para Transportadora", ex3.Message);
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Entregue Status Atualização: Cancelada", ex4.Message);
        }

        [Fact]
        public async void SaleService_SaleUpdates_Fails_CanceledStatus()
        {
            // Arrange
            List<SaleItemsCreateListDto> listItems = new List<SaleItemsCreateListDto>() { new SaleItemsCreateListDto { ProductId = 1, Amount = 5, UnitPrice = 1 } };
            var saleCreate = new SaleCreateDto { SellerId = 1, DatePurchase = DateTime.Now, saleItems = listItems };
            var resultCreate = await _saleService.Insert(saleCreate);
            await _saleService.Update(new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.CANCELED });            

            var saleUpdate = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.AWAITING_PAYMENT };
            var saleUpdate1 = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.PAYMENT_ACCEPT };
            var saleUpdate2 = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.SEND_TO_CARRIER };
            var saleUpdate3 = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = StatusSale.DELIVERED };

            // Act
            var ex1 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate));
            var ex2 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate1));
            var ex3 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate2));
            var ex4 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate3));

            // Assert
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Cancelada Status Atualização: Aguardando pagamento", ex1.Message);
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Cancelada Status Atualização: Pagamento Aprovado", ex2.Message);
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Cancelada Status Atualização: Enviado para Transportadora", ex3.Message);
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Cancelada Status Atualização: Entregue", ex4.Message);
        }

        [Fact]
        public async void SaleService_SaleUpdates_Fails_InvalidStatus()
        {
            // Arrange
            List<SaleItemsCreateListDto> listItems = new List<SaleItemsCreateListDto>() { new SaleItemsCreateListDto { ProductId = 1, Amount = 5, UnitPrice = 1 } };
            var saleCreate = new SaleCreateDto { SellerId = 1, DatePurchase = DateTime.Now, saleItems = listItems };
            var resultCreate = await _saleService.Insert(saleCreate);

            var saleUpdate = new SaleUpdateDto { Id = resultCreate.Id, StatusSale = (StatusSale)6 };            

            // Act
            var ex1 = await Assert.ThrowsAsync<Exception>(() => _saleService.Update(saleUpdate));            

            // Assert
            Assert.Equal("Erro ao tentar alterar o status da Venda. Status Venda: Aguardando pagamento Status Atualização: Status inválido", ex1.Message);
        }

        [Fact]
        public async void SaleService_SaleGetById()
        {
            // Arrange
            var saleId = 1;

            // Act
            var resultGet = await _saleService.GetById(saleId);

            // Assert
            Assert.NotNull(resultGet);
            Assert.Equal(saleId, resultGet.Id);
            Assert.True(resultGet.DateCreated > DateTime.MinValue);
        }

        [Fact]
        public async void SaleService_SaleGetCompleteById()
        {
            // Arrange
            var saleId = 1;

            // Act
            var resultGet = await _saleService.GetCompleteById(saleId);

            // Assert
            Assert.NotNull(resultGet);
            Assert.Equal(saleId, resultGet.Id);
            Assert.True(resultGet.DateCreated > DateTime.MinValue);
            Assert.NotNull(resultGet.Seller);
            Assert.True(resultGet.TotalSale > 0);
            Assert.True(resultGet.SaleItems.Count() > 0);
        }

        [Fact]
        public async void SaleService_SalesGetAll()
        {
            // Arrange

            // Act
            var result = await _saleService.GetAll();

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Count() > 0);
        }

        [Fact]
        public async void SaleService_SalesGetCompleteAll()
        {
            // Arrange

            // Act
            var result = await _saleService.GetAllComplete();

            var itemResult = result.FirstOrDefault();

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Count() > 0);
            Assert.NotNull(itemResult.Seller);
            Assert.True(itemResult.TotalSale > 0);
            Assert.True(itemResult.SaleItems.Count() > 0);
        }
    }
}
