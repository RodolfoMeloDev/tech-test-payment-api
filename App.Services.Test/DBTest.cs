﻿using App.CrossCutting.Mappings;
using App.Data.Context;
using App.Data.Implementations;
using App.Domain.Interfaces.Repositories;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Services.Test
{
    public class DBTest
    {
        public ServiceProvider ServiceProvider { get; private set; }
        public SalesContext _context;
        public readonly IMapper _mapper;

        public DBTest()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddDbContext<SalesContext>();

            ServiceProvider = serviceCollection.BuildServiceProvider();
            _context = ServiceProvider.GetService<SalesContext>();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new DtoToModelProfile());
                cfg.AddProfile(new ModelToEntityProfile());
                cfg.AddProfile(new EntityToDtoProfile());
            });

            _mapper = config.CreateMapper();
        }
    }
}
