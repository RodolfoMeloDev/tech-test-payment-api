﻿using App.Data.Implementations;
using App.Domain.Dtos.Product;
using App.Services.Services;
using Xunit;

namespace App.Services.Test
{
    public class ProductServiceTests : DBTest
    {
        private ProductImplementation _repository;

        public ProductServiceTests()
        {            
            _repository = new ProductImplementation(_context);
        }        

        [Fact]
        public async void ProductServices_ProductsCreated_Success()
        {
            // Arrange
            var product = new ProductCreateDto { Name = "Test", UnitPrice = 10 };
            var service = new ProductService(_repository, _mapper);

            // Act
            var result = await service.Insert(product);

            // Assert
            Assert.NotNull(result);
            Assert.True(result.Id > 0);
            Assert.True(result.DateCreated != DateTime.MinValue);
            Assert.Equal(product.Name.ToUpper(), result.Name);
            Assert.Equal(product.UnitPrice, result.UnitPrice);
        }

        [Fact]
        public async Task ProductServices_ProductsCreated_Fail()
        {
            // Arrange
            var product = new ProductCreateDto { Name = "Test", UnitPrice = 0 };

            var service = new ProductService(_repository, _mapper);

            // Act
            var ex = await Assert.ThrowsAsync<Exception>(() => service.Insert(product));

            // Assert
            Assert.Contains("Preço Unitário inválido para o produto", ex.Message);

        }

        [Fact]
        public async void ProductServices_ProductsUpdate_Success()
        {
            // Arrange
            var product = new ProductUpdateDto { Id = 1, Name = "Test", UnitPrice = 10 };
            var service = new ProductService(_repository, _mapper);

            // Act
            var result = await service.Update(product);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(product.Id, result.Id);
            Assert.Equal(product.Name.ToUpper(), result.Name);
            Assert.Equal(product.UnitPrice, result.UnitPrice);
            Assert.NotNull(result.DateUpdated);
        }

        [Fact]
        public async Task ProductServices_ProductsUpdate_Fail()
        {
            // Arrange
            var product = new ProductUpdateDto { Id = 1, Name = "Test", UnitPrice = 0 };

            var service = new ProductService(_repository, _mapper);

            // Act
            var ex = await Assert.ThrowsAsync<Exception>(() => service.Update(product));

            // Assert
            Assert.Contains("Preço Unitário inválido para o produto", ex.Message);

        }


        [Fact]
        public async void ProductServices_ProductsGetById()
        {
            // Arrange
            var productId = 1;
            var product = new ProductCreateDto { Name = "Test", UnitPrice = 10 };
            var service = new ProductService(_repository, _mapper);

            // Act
            var result = await service.Insert(product);

            Assert.NotNull(result);

            var resultGet = await service.GetById(productId);

            // Assert
            Assert.NotNull(resultGet);
            Assert.Equal(productId, resultGet.Id);
            Assert.Equal(product.Name.ToUpper(), resultGet.Name);
            Assert.Equal(product.UnitPrice, resultGet.UnitPrice);
            Assert.True(resultGet.DateCreated > DateTime.MinValue);
            Assert.True(resultGet.DateUpdated == null);
        }

        [Fact]
        public async void ProductServices_ProductsGetAll()
        {
            // Arrange
            var product = new ProductCreateDto { Name = "Test", UnitPrice = 10 };
            var service = new ProductService(_repository, _mapper);

            // Act
            for (int i = 0; i < 3; i++)
            {
                await service.Insert(product);
            }

            var result = await service.GetAll();

            // Assert
            Assert.NotNull(result);                        
            Assert.True(result.Count() > 0);
        }

        [Fact]
        public async void ProductServices_ProductsDeleteById_Success()
        {
            // Arrange
            var product = new ProductCreateDto { Name = "Test", UnitPrice = 10 };
            var service = new ProductService(_repository, _mapper);

            // Act
            var result = await service.Insert(product);
            Assert.NotNull(result);

            var resultDelete = await service.Delete(result.Id);
            
            // Assert
            Assert.True(resultDelete);
        }

        [Fact]
        public async void ProductServices_ProductsDeleteById_Fail()
        {
            // Arrange
            var product = new ProductCreateDto { Name = "Test", UnitPrice = 10 };
            var service = new ProductService(_repository, _mapper);

            // Act
            var result = await service.Insert(product);
            Assert.NotNull(result);

            var resultDelete = await service.Delete(9999);

            // Assert
            Assert.False(resultDelete);
        }

    }
}
