using System;
using System.Net;
using System.Threading.Tasks;
using App.Domain.Dtos.Product;
using App.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace App.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _service;

        public ProductController(IProductService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("{id}", Name = "GetProductWithId")]
        public async Task<IActionResult> GetProductById(int id){
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                return Ok(await _service.GetById(id));
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllProducts(){
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                return Ok(await _service.GetAll());
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> InsertProduct([FromBody] ProductCreateDto product)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _service.Insert(product);

                if (result == null)
                    return BadRequest();

                var _url = Url.Link("GetProductWithId", new { id = result.Id} );
                
                return Created(new Uri(_url), result);
            }
            catch (System.Exception)
            {
                
                throw;
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateProduct([FromBody] ProductUpdateDto product)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _service.Update(product);

                if (result != null)
                    return Ok(result);

                return BadRequest();
            }
            catch (ArgumentException e)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteProduct(int id)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                return Ok(await _service.Delete(id));
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}