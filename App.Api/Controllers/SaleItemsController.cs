using System;
using System.Net;
using System.Threading.Tasks;
using App.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace App.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SaleItemsController : ControllerBase
    {
        private readonly ISaleItemsService _service;

        public SaleItemsController(ISaleItemsService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("{id}")]
        public async Task<IActionResult> GetSaleItemById(int id){
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                return Ok(await _service.GetById(id));
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllSaleItems(){
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                return Ok(await _service.GetAll());
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpGet]
        [Route("Sale/{saleId}")]
        public async Task<IActionResult> GetSaleItemsBySale(int saleId){
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                return Ok(await _service.GetBySaleId(saleId));
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpGet]
        [Route("Sale/{saleId}/TotalSale")]
        public async Task<IActionResult> GetTotalSale(int saleId){
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                return Ok(await _service.GetTotalSale(saleId));
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}