using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using App.Domain.Dtos.Sale;
using App.Domain.Dtos.SaleItems;
using App.Domain.Enums;
using App.Domain.Interfaces.Services;
using Microsoft.AspNetCore.Mvc;

namespace App.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly ISaleService _saleService;

        public SaleController(ISaleService saleService)
        {
            _saleService = saleService;
        }

        [HttpGet]
        [Route("{id}", Name = "GetSaleWithId")]
        public async Task<IActionResult> GetSeleById(int id){
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                return Ok(await _saleService.GetById(id));
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpGet]
        [Route("Complete/{id}")]
        public async Task<IActionResult> GetSeleCompleteById(int id){
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                return Ok(await _saleService.GetCompleteById(id));
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllSales(){
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                return Ok(await _saleService.GetAll());
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpGet]
        [Route("Complete")]
        public async Task<IActionResult> GetAllCompleteSales(){
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                return Ok(await _saleService.GetAllComplete());
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpPatch]
        public async Task<IActionResult> UpdateStatusSale([FromBody] SaleUpdateDto sale)
        {
            try
            {
                if (!ModelState.IsValid)
                    BadRequest();

                return Ok(await _saleService.Update(sale));

            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> InsertSale([FromBody] SaleCreateDto sale)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _saleService.Insert(sale);

                if (result == null)
                    return BadRequest();

                var _url = Url.Link("GetSaleWithId", new { id = result.Id} );
                
                return Created(new Uri(_url), result);
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpPost]
        [Route("SellerByCPF")]
        public async Task<IActionResult> InsertSaleBySellerCPF([FromBody] SaleCreateByCPFDto sale)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _saleService.InsertBySellerCPF(sale);

                if (result == null)
                    return BadRequest();

                var _url = Url.Link("GetSaleWithId", new { id = result.Id} );
                
                return Created(new Uri(_url), result);
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }

        [HttpPost]
        [Route("SellerByNewSeller")]
        public async Task<IActionResult> InsertSaleByNewSeller([FromBody] SaleCreateNewSellerDto sale)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest();

                var result = await _saleService.InsertByNewSeller(sale);

                if (result == null)
                    return BadRequest();

                var _url = Url.Link("GetSaleWithId", new { id = result.Id} );
                
                return Created(new Uri(_url), result);
            }
            catch (ArgumentException e)
            {                
                return StatusCode((int)HttpStatusCode.InternalServerError, e.Message);
            }
        }
    }
}