using System;
using App.CrossCutting.DependencyInjection;
using App.CrossCutting.Mappings;
using App.Data.Context;
using App.Data.Seed;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

ConfigureService.ConfigureDependenciesService(builder.Services);
ConfigureRepository.ConfigureDependenciesRepository(builder.Services);

var config = new MapperConfiguration(cfg =>
{
    cfg.AddProfile(new DtoToModelProfile());
    cfg.AddProfile(new ModelToEntityProfile());
    cfg.AddProfile(new EntityToDtoProfile());
});

IMapper mapper = config.CreateMapper();

builder.Services.AddSingleton(mapper);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(
    s => {
        s.SwaggerDoc("v1", new OpenApiInfo {
            Title = "Prova Teste da Pottencial Segurado",
            Description = "Criado alguns EndPoints para que o backEnd possa realizar uma Venda, Atualização de Status da Venda e Consultas os Dados",
            Contact = new OpenApiContact 
            {
                Name = "Rodolfo Lopes de Melo",
                Email = "rodollopes@gmail.com" ,
                Url = new Uri("https://github.com/RodolfoMeloDev")
            }
        });
    }
);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

using (var service = app.Services.GetRequiredService<IServiceScopeFactory>().CreateScope())
using (var context = service.ServiceProvider.GetService<SalesContext>())
{
    if (context != null)
    {
        DatasInitial.InsertDatasInitial(context);
    }
}

app.Run();
