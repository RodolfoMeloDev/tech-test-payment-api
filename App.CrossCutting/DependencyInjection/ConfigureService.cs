using App.Domain.Interfaces.Services;
using App.Services.Services;
using Microsoft.Extensions.DependencyInjection;

namespace App.CrossCutting.DependencyInjection
{
    public class ConfigureService
    {
        public static void ConfigureDependenciesService(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<ISellerService, SellerService>();
            serviceCollection.AddTransient<IProductService, ProductService>();
            serviceCollection.AddTransient<ISaleService, SaleService>();
            serviceCollection.AddTransient<ISaleItemsService, SaleItemsService>();
        }
    }
}