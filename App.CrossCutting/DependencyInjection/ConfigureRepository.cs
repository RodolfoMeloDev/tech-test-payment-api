using App.Data.Context;
using App.Data.Implementations;
using App.Data.Repository;
using App.Domain.Interfaces.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace App.CrossCutting.DependencyInjection
{
    public class ConfigureRepository
    {
        public static void ConfigureDependenciesRepository(IServiceCollection serviceCollection)
        {   
            serviceCollection.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));
            serviceCollection.AddScoped<ISellerRepository, SellerImplementation>();
            serviceCollection.AddScoped<IProductRepository, ProductImplementation>();
            serviceCollection.AddScoped<ISaleRepository, SaleImplementation>();
            serviceCollection.AddScoped<ISaleItemsRepository, SaleItemsImplementation>();

            serviceCollection.AddDbContext<SalesContext>();
        }
    }
}