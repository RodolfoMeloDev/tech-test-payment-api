using App.Domain.Dtos.Product;
using App.Domain.Dtos.Sale;
using App.Domain.Dtos.SaleItems;
using App.Domain.Dtos.Seller;
using App.Domain.Models;
using AutoMapper;

namespace App.CrossCutting.Mappings
{
    public class DtoToModelProfile : Profile
    {
        public DtoToModelProfile()
        {
            #region Seller
            CreateMap<SellerModel, SellerDto>().ReverseMap();
            CreateMap<SellerModel, SellerCreateDto>().ReverseMap();
            CreateMap<SellerModel, SellerUpdateDto>().ReverseMap();
            #endregion

            #region Product
            CreateMap<ProductModel, ProductDto>().ReverseMap();
            CreateMap<ProductModel, ProductCreateDto>().ReverseMap();
            CreateMap<ProductModel, ProductUpdateDto>().ReverseMap();
            #endregion

            #region Sale
            CreateMap<SaleModel, SaleDto>().ReverseMap();
            CreateMap<SaleModel, SaleCompleteDto>().ReverseMap();
            CreateMap<SaleModel, SaleCreateDto>().ReverseMap();
            CreateMap<SaleModel, SaleCreateByCPFDto>().ReverseMap();
            CreateMap<SaleModel, SaleCreateNewSellerDto>().ReverseMap();
            CreateMap<SaleModel, SaleUpdateDto>().ReverseMap();
            #endregion

            #region SaleItems
            CreateMap<SaleItemsModel, SaleItemsDto>().ReverseMap();
            CreateMap<SaleItemsModel, SaleItemsCreateDto>().ReverseMap();   
            CreateMap<SaleItemsModel, SaleItemsCreateListDto>().ReverseMap();                                   
            #endregion
        }
    }
}