using App.Domain.Dtos.Product;
using App.Domain.Dtos.Sale;
using App.Domain.Dtos.SaleItems;
using App.Domain.Dtos.Seller;
using App.Domain.Entities;
using AutoMapper;

namespace App.CrossCutting.Mappings
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile()
        {
            # region Seller
            CreateMap<SellerDto, SellerEntity>().ReverseMap();
            CreateMap<SellerCreateDtoResult, SellerEntity>().ReverseMap();
            CreateMap<SellerUpdateDtoResult, SellerEntity>().ReverseMap();
            # endregion

            # region Product
            CreateMap<ProductDto, ProductEntity>().ReverseMap();
            CreateMap<ProductCreateDtoResult, ProductEntity>().ReverseMap();
            CreateMap<ProductUpdateDtoResult, ProductEntity>().ReverseMap();
            # endregion

            # region Sale
            CreateMap<SaleDto, SaleEntity>().ReverseMap();
            CreateMap<SaleCompleteDto, SaleEntity>().ReverseMap();
            CreateMap<SaleCreateDtoResult, SaleEntity>().ReverseMap();
            CreateMap<SaleUpdateDtoResult, SaleEntity>().ReverseMap();
            # endregion

            # region SaleItems
            CreateMap<SaleItemsDto, SaleItemsEntity>().ReverseMap();
            CreateMap<SaleItemsCreateDtoResult, SaleItemsEntity>().ReverseMap();
            # endregion
        }
    }
}