using App.Domain.Entities;
using App.Domain.Models;
using AutoMapper;

namespace App.CrossCutting.Mappings
{
    public class ModelToEntityProfile : Profile
    {
        public ModelToEntityProfile()
        {
            CreateMap<SellerEntity, SellerModel>().ReverseMap();
            CreateMap<ProductEntity, ProductModel>().ReverseMap();
            CreateMap<SaleEntity, SaleModel>().ReverseMap();
            CreateMap<SaleItemsEntity, SaleItemsModel>().ReverseMap();
        }
    }
}