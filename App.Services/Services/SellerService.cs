using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Domain.Dtos.Seller;
using App.Domain.Entities;
using App.Domain.Interfaces.Repositories;
using App.Domain.Interfaces.Services;
using App.Domain.Models;
using AutoMapper;

namespace App.Services.Services
{
    public class SellerService : ISellerService
    {
        private ISellerRepository _repository;
        private readonly IMapper _mapper;

        public SellerService(ISellerRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<bool> Delete(int id)
        {
            return await _repository.DeleteAsync(id);
        }

        public async Task<IEnumerable<SellerDto>> GetAll()
        {
            var listEntity = await _repository.SelectAllAsync();

            return _mapper.Map<IEnumerable<SellerDto>>(listEntity);
        }

        public async Task<SellerDto> GetById(int id)
        {
            var entity = await _repository.SelectAsync(id);

            return _mapper.Map<SellerDto>(entity);
        }

        public async Task<SellerCreateDtoResult> Insert(SellerCreateDto seller)
        {
            var existSellerCpf = await _repository.ExistSellerWithCPF(seller.Cpf);

            if (existSellerCpf != null)
                throw new Exception($"Já existe um vendedor cadastro com o CPF: {seller.Cpf}");

            if (!IsValidCPF(seller.Cpf))
                throw new Exception($"O CPF: {seller.Cpf} não é válido!");

            var model = _mapper.Map<SellerModel>(seller);
            var entity = _mapper.Map<SellerEntity>(model);
            var result = await _repository.InsertAsync(entity);

            return _mapper.Map<SellerCreateDtoResult>(result);
        }

        public async Task<SellerUpdateDtoResult> Update(SellerUpdateDto seller)
        {
            var existSellerCpf = await _repository.ExistSellerWithCPF(seller.Cpf);

            if (existSellerCpf != null && !seller.Id.Equals(existSellerCpf.Id))
                throw new Exception("O código do vendedor informado é inválido para o CPF");
                
            if (!IsValidCPF(seller.Cpf))
                throw new Exception($"O CPF: {seller.Cpf} não é válido!");

            var model = _mapper.Map<SellerModel>(seller);
            var entity = _mapper.Map<SellerEntity>(model);
            var result = await _repository.UpdateAsync(entity);

            return _mapper.Map<SellerUpdateDtoResult>(result);
        }

        private bool IsValidCPF(string cpf){
            var numbersCpf = cpf.Replace(".", "").Replace("-", "");
            int valueCalculeCpf = 0;
            int valueDigitOne = 0;
            int valueDigitTwo = 0;
            bool existDiferentNumbers = false;
            char lastChar = numbersCpf[0];

            // valida se existem somente caracteres iguais, Exemplo 111.111.111-11
            // Neste caso inválida o CPF
            for (int i = 1; i < numbersCpf.Length; i++)
            {
                if (!lastChar.Equals(numbersCpf[i])){
                    existDiferentNumbers = true;
                    break;
                }
            }

            if (!existDiferentNumbers)
                return false;

            for (int i = 0; i < numbersCpf.Length - 2; i++)
            {
                valueCalculeCpf += Convert.ToInt16(Convert.ToString(numbersCpf[i])) * (numbersCpf.Length - i - 1);
            }

            valueDigitOne = (valueCalculeCpf % 11) < 2 ? 0 : 11 - (valueCalculeCpf % 11);

            if (Convert.ToInt16(Convert.ToString(numbersCpf[9])) != valueDigitOne)
                return false;

            valueCalculeCpf = 0;
            for (int i = 0; i < numbersCpf.Length - 1; i++)
            {
                valueCalculeCpf += Convert.ToInt16(Convert.ToString(numbersCpf[i])) * (numbersCpf.Length - i);
            }

            valueDigitTwo = (valueCalculeCpf % 11) < 2 ? 0 : 11 - (valueCalculeCpf % 11);

            if (Convert.ToInt16(Convert.ToString(numbersCpf[10])) != valueDigitTwo)
                return false;

            return true;
        }

        public async Task<SellerDto> GetByCPF(string cpf)
        {
            var entity = await _repository.ExistSellerWithCPF(cpf);

            return _mapper.Map<SellerDto>(entity);
        }
    }
}