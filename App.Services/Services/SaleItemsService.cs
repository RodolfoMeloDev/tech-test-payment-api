using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Domain.Dtos.SaleItems;
using App.Domain.Entities;
using App.Domain.Interfaces.Repositories;
using App.Domain.Interfaces.Services;
using App.Domain.Models;
using AutoMapper;

namespace App.Services.Services
{
    public class SaleItemsService : ISaleItemsService
    {
        private ISaleItemsRepository _repository;
    
        private readonly IMapper _mapper;

        public SaleItemsService(ISaleItemsRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<bool> DeleteById(int id)
        {
            return await _repository.DeleteAsync(id);
        }

        public async Task<bool> DeleteBySaleId(int saleId)
        {
            return await _repository.DeleteBySaleId(saleId);
        }
        public async Task<SaleItemsDto> GetById(int id)
        {
            var entity = await _repository.SelectAsync(id);

            return _mapper.Map<SaleItemsDto>(entity);
        }

        public async Task<IEnumerable<SaleItemsDto>> GetBySaleId(int saleId)
        {
            var listEntity = await _repository.GetBySaleId(saleId);

            return _mapper.Map<IEnumerable<SaleItemsDto>>(listEntity);
        }

        public async Task<IEnumerable<SaleItemsDto>> GetAll()
        {
            var listEntity = await _repository.SelectAllAsync();

            return _mapper.Map<IEnumerable<SaleItemsDto>>(listEntity);
        }

        public async Task<decimal> GetTotalSale(int saleId)
        {
            return await _repository.TotalSale(saleId);
        }

        public async Task<SaleItemsCreateDtoResult> Insert(SaleItemsCreateDto saleItem)
        {
            if (saleItem.UnitPrice <= 0)
                throw new Exception($"Preço Unitário inválido para o produto");

            if (saleItem.Amount <= 0)
                throw new Exception($"Quantidade informada deve ser maior que zero");

            var model = _mapper.Map<SaleItemsModel>(saleItem);
            var entity = _mapper.Map<SaleItemsEntity>(model);
            var result = await _repository.InsertAsync(entity);

            return _mapper.Map<SaleItemsCreateDtoResult>(result);
        }

        public async Task<IEnumerable<SaleItemsCreateDtoResult>> InsertListItems(int saleId, IEnumerable<SaleItemsCreateListDto> listItems)
        {
            if (listItems.Count() == 0)
                throw new Exception($"Deve ser informado ao menos 1 item para concluir a compra.");            

            IList<SaleItemsEntity> listEntity = new List<SaleItemsEntity>();

            foreach (var item in listItems)
            {
                if (item.UnitPrice <= 0)
                    throw new Exception($"Preço Unitário inválido para o produto: {item.ProductId}");

                if (item.Amount <= 0)
                    throw new Exception($"Quantidade informada deve ser maior que zero para o produto: {item.ProductId}");

                var model = _mapper.Map<SaleItemsModel>(item);
                model.SaleId = saleId;
                model.Status = Domain.Enums.StatusRegister.ACTIVE;

                var entity = _mapper.Map<SaleItemsEntity>(model);
                listEntity.Add(entity);
            }

            var result = await _repository.InsertListItems(listEntity);

            return _mapper.Map<IEnumerable<SaleItemsCreateDtoResult>>(result);
        }
    }
}