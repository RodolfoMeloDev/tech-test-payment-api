using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Domain.Dtos.Product;
using App.Domain.Entities;
using App.Domain.Interfaces.Repositories;
using App.Domain.Interfaces.Services;
using App.Domain.Models;
using AutoMapper;

namespace App.Services.Services
{
    public class ProductService : IProductService
    {
        private IProductRepository _repository;
        private readonly IMapper _mapper;

        public ProductService(IProductRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public async Task<bool> Delete(int id)
        {
            return await _repository.DeleteAsync(id);
        }

        public async Task<IEnumerable<ProductDto>> GetAll()
        {
            var listEntity = await _repository.SelectAllAsync();

            return _mapper.Map<IEnumerable<ProductDto>>(listEntity);
        }

        public async Task<ProductDto> GetById(int id)
        {
            var entity = await _repository.SelectAsync(id);

            return _mapper.Map<ProductDto>(entity);
        }

        public async Task<ProductCreateDtoResult> Insert(ProductCreateDto product)
        {
            if (product.UnitPrice <= 0)
                throw new Exception($"Preço Unitário inválido para o produto {product.Name}");

            var model = _mapper.Map<ProductModel>(product);
            var entity = _mapper.Map<ProductEntity>(model);
            var result = await _repository.InsertAsync(entity);

            return _mapper.Map<ProductCreateDtoResult>(result);
        }

        public async Task<ProductUpdateDtoResult> Update(ProductUpdateDto product)
        {
            if (product.UnitPrice <= 0)
                throw new Exception($"Preço Unitário inválido para o produto {product.Name}");
                
            var model = _mapper.Map<ProductModel>(product);
            var entity = _mapper.Map<ProductEntity>(model);
            var result = await _repository.UpdateAsync(entity);

            return _mapper.Map<ProductUpdateDtoResult>(result);
        }
    }
}