using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Domain.Dtos.Sale;
using App.Domain.Dtos.Seller;
using App.Domain.Entities;
using App.Domain.Enums;
using App.Domain.Interfaces.Repositories;
using App.Domain.Interfaces.Services;
using App.Domain.Models;
using AutoMapper;

namespace App.Services.Services
{
    public class SaleService : ISaleService
    {
        private ISaleRepository _saleRepository;
        private ISellerService _sellerService;
        private ISaleItemsService _saleItemsService;
        private readonly IMapper _mapper;

        public SaleService(ISaleRepository saleRepository, ISellerService sellerService, ISaleItemsService saleItemsService, IMapper mapper)
        {
            _saleRepository = saleRepository;
            _sellerService = sellerService;
            _saleItemsService = saleItemsService;
            _mapper = mapper;
        }

        public async Task<IEnumerable<SaleDto>> GetAll()
        {
            var listEntity = await _saleRepository.SelectAllAsync();

            return _mapper.Map<IEnumerable<SaleDto>>(listEntity);
        }

        public async Task<IEnumerable<SaleCompleteDto>> GetAllComplete()
        {
            var listEntity = await _saleRepository.SelectAllCompleteAsync();

            var resultList = _mapper.Map<IEnumerable<SaleCompleteDto>>(listEntity);

            foreach (var item in resultList)
            {
                item.TotalSale = await _saleItemsService.GetTotalSale(item.Id);
                item.SaleItems = await _saleItemsService.GetBySaleId(item.Id);
            }

            return resultList;
        }

        public async Task<SaleDto> GetById(int id)
        {
            var entity = await _saleRepository.SelectAsync(id);

            return _mapper.Map<SaleDto>(entity);
        }

        public async Task<SaleCompleteDto> GetCompleteById(int id)
        {
            var entity = await _saleRepository.SelectCompleteAsync(id);

            var result = _mapper.Map<SaleCompleteDto>(entity);

            result.TotalSale = await _saleItemsService.GetTotalSale(id);
            result.SaleItems = await _saleItemsService.GetBySaleId(id);

            return result;
        }

        public async Task<SaleCompleteDto> InsertByNewSeller(SaleCreateNewSellerDto sale)
        {
            SellerCreateDto newSeller = new SellerCreateDto() { Name = sale.Name, Cpf = sale.Cpf, Email = sale.Email, Phone = sale.Phone };
            var resultNewSeller = await _sellerService.Insert(newSeller);

            if (resultNewSeller == null)
                throw new Exception("Erro ao cadastrar a Venda. Não foi possível cadastrar o novo vendedor.");

            if (sale.DatePurchase < DateTime.Today)
                throw new Exception($"A data da compra não pode ser inferior a data atual.");

            if (sale.saleItems.Count() == 0)
                throw new Exception($"A compra deve possuir ao menos 1 produto");

            SaleCreateDto newSale = new SaleCreateDto() { SellerId = resultNewSeller.Id, DatePurchase = sale.DatePurchase };

            var model = _mapper.Map<SaleModel>(newSale);
            var entity = _mapper.Map<SaleEntity>(model);
            var resultSale = await _saleRepository.InsertAsync(entity);

            await _saleItemsService.InsertListItems(resultSale.Id, sale.saleItems);

            var returnData = _mapper.Map<SaleCompleteDto>(await _saleRepository.SelectCompleteAsync(resultSale.Id));
            returnData.TotalSale = await _saleItemsService.GetTotalSale(resultSale.Id);
            returnData.SaleItems = await _saleItemsService.GetBySaleId(resultSale.Id);

            return returnData;
        }

        public async Task<SaleCompleteDto> InsertBySellerCPF(SaleCreateByCPFDto sale)
        {
            var seller = await _sellerService.GetByCPF(sale.Cpf);

            if (seller == null)
                throw new Exception($"Não foi encontrado o vendedor com o CPF: {sale.Cpf}");

            if (sale.DatePurchase < DateTime.Today)
                throw new Exception($"A data da compra não pode ser inferior a data atual.");

            if (sale.saleItems.Count() == 0)
                throw new Exception($"A compra deve possuir ao menos 1 produto");

            SaleCreateDto newSale = new SaleCreateDto() { SellerId = seller.Id, DatePurchase = sale.DatePurchase };

            var model = _mapper.Map<SaleModel>(newSale);
            var entity = _mapper.Map<SaleEntity>(model);
            var resultSale = await _saleRepository.InsertAsync(entity);

            await _saleItemsService.InsertListItems(resultSale.Id, sale.saleItems);

            var returnData = _mapper.Map<SaleCompleteDto>(await _saleRepository.SelectCompleteAsync(resultSale.Id));
            returnData.TotalSale = await _saleItemsService.GetTotalSale(resultSale.Id);
            returnData.SaleItems = await _saleItemsService.GetBySaleId(resultSale.Id);

            return returnData;
        }

        public async Task<SaleCompleteDto> Insert(SaleCreateDto sale)
        {
            if (sale.DatePurchase < DateTime.Today)
                throw new Exception($"A data da compra não pode ser inferior a data atual.");

            if (sale.saleItems.Count() == 0)
                throw new Exception($"A compra deve possuir ao menos 1 produto");

            var model = _mapper.Map<SaleModel>(sale);
            var entity = _mapper.Map<SaleEntity>(model);
            var resultSale = await _saleRepository.InsertAsync(entity);

            await _saleItemsService.InsertListItems(resultSale.Id, sale.saleItems);

            var returnData = _mapper.Map<SaleCompleteDto>(await _saleRepository.SelectCompleteAsync(resultSale.Id));
            returnData.TotalSale = await _saleItemsService.GetTotalSale(resultSale.Id);
            returnData.SaleItems = await _saleItemsService.GetBySaleId(resultSale.Id);

            return returnData;
        }

        public async Task<SaleUpdateDtoResult> Update(SaleUpdateDto sale)
        {
            var oldSale = await _saleRepository.SelectAsync(sale.Id);

            if (oldSale.StatusSale == sale.StatusSale)
                throw new Exception("A venda já está no status indicado.");

            if (!IsValidNewStatusSale(oldSale.StatusSale, sale.StatusSale))
                throw new Exception($"Erro ao tentar alterar o status da Venda. Status Venda: { ReturnNameStatusSale(oldSale.StatusSale) } Status Atualização: { ReturnNameStatusSale(sale.StatusSale) }");
                
            var model = _mapper.Map<SaleModel>(sale);

            model.DatePurchase = oldSale.DatePurchase;
            model.DateCreated = (DateTime)oldSale.DateCreated;
            model.SellerId = oldSale.SellerId;
            model.Status = oldSale.Status;

            var entity = _mapper.Map<SaleEntity>(model);
            var result = await _saleRepository.UpdateAsync(entity);

            return _mapper.Map<SaleUpdateDtoResult>(result);
        }

        private bool IsValidNewStatusSale(StatusSale oldStatus, StatusSale newStatus)
        {
            if (oldStatus == StatusSale.AWAITING_PAYMENT && newStatus != StatusSale.PAYMENT_ACCEPT && newStatus != StatusSale.CANCELED )
                return false;

            if (oldStatus == StatusSale.PAYMENT_ACCEPT && newStatus != StatusSale.SEND_TO_CARRIER && newStatus != StatusSale.CANCELED )
                return false;

            if (oldStatus == StatusSale.SEND_TO_CARRIER && newStatus != StatusSale.DELIVERED )
                return false;

            if (oldStatus == StatusSale.DELIVERED && newStatus != StatusSale.DELIVERED)
                return false;

            if (oldStatus == StatusSale.CANCELED && newStatus != StatusSale.CANCELED)
                return false;

            if (newStatus != StatusSale.AWAITING_PAYMENT && newStatus != StatusSale.PAYMENT_ACCEPT && newStatus != StatusSale.SEND_TO_CARRIER &&
                newStatus != StatusSale.DELIVERED && newStatus != StatusSale.CANCELED)
                return false;

            return true;
        }

        private string ReturnNameStatusSale(StatusSale status){
            
            if (status == StatusSale.AWAITING_PAYMENT)
                return "Aguardando pagamento";

            if (status == StatusSale.PAYMENT_ACCEPT)
                return "Pagamento Aprovado";

            if (status == StatusSale.SEND_TO_CARRIER)
                return "Enviado para Transportadora";

            if (status == StatusSale.DELIVERED)
                return "Entregue";

            if (status == StatusSale.CANCELED)
                return "Cancelada";

            return "Status inválido";
        }
    }
}