using App.Domain.Enums;
using System;

namespace App.Domain.Models
{
    public class BaseModel
    {
        private int _id;
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private StatusRegister _status;
        public StatusRegister Status
        {
            get { return _status; }
            set { _status = value != StatusRegister.INACTIVE && value != StatusRegister.ACTIVE ? StatusRegister.ACTIVE : value; }
        }

        private DateTime _dateCreated;
        public DateTime DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = value == DateTime.MinValue ? DateTime.Now : value; }
        }

        private DateTime _dateUpdated;
        public DateTime DateUpdated
        {
            get { return _dateUpdated; }
            set { _dateUpdated = value; }
        }
    }
}