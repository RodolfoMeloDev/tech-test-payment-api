namespace App.Domain.Models
{
    public class ProductModel : BaseModel
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value.ToUpper(); }
        }

        private decimal _unitPrice;
        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set { _unitPrice = value; }
        }
        
    }
}