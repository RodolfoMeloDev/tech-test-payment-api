using System;
using App.Domain.Enums;

namespace App.Domain.Models
{
    public class SaleModel : BaseModel
    {
        private int _sellerId;
        public int SellerId
        {
            get { return _sellerId; }
            set { _sellerId = value; }
        }

        private DateTime _datePurchase;
        public DateTime DatePurchase
        {
            get { return _datePurchase; }
            set { _datePurchase = value; }
        }
        
        private StatusSale _statusSale;
        public StatusSale StatusSale
        {
            get { return _statusSale; }
            set { _statusSale = value; }
        }
        
        
        
    }
}