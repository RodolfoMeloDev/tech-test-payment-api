namespace App.Domain.Models
{
    public class SaleItemsModel : BaseModel
    {
        private int _saleId;
        public int SaleId
        {
            get { return _saleId; }
            set { _saleId = value; }
        }
        
        private int _productId;
        public int ProductId
        {
            get { return _productId; }
            set { _productId = value; }
        }

        private int _amount;
        public int Amount
        {
            get { return _amount; }
            set { _amount = value; }
        }
        
        private decimal _unitPrice;
        public decimal UnitPrice
        {
            get { return _unitPrice; }
            set { _unitPrice = value; }
        }

        private decimal _totalPrice;
        public decimal TotalPrice
        {
            get { return _amount * _unitPrice; }
            set { _totalPrice = _amount * _unitPrice; }
        }
                    
    }
}