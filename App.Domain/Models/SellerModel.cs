namespace App.Domain.Models
{
    public class SellerModel : BaseModel
    {
        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = value.ToUpper(); }
        }
        
        private string _cpf;
        public string Cpf
        {
            get { return _cpf; }
            set { _cpf = value; }
        }
        
        private string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value.ToUpper(); }
        }

        private string _phone;
        public string Phone
        {
            get { return _phone; }
            set { _phone = value; }
        }
        
        
    }
}