using System.ComponentModel.DataAnnotations;

namespace App.Domain.Entities
{
    public class SellerEntity : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [MaxLength(14)]
        public string Cpf { get; set; }

        [MaxLength(100)]
        public string Email { get; set; }

        [MaxLength(16)]
        public string Phone { get; set; }
    }
}