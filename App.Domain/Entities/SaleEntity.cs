using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using App.Domain.Enums;

namespace App.Domain.Entities
{
    public class SaleEntity : BaseEntity
    {
        [Required]
        [ForeignKey("Fk_Seller_Venda")]
        public int SellerId { get; set; }

        public SellerEntity Seller { get; set; }

        [Required]
        public DateTime DatePurchase { get; set; }

        [Required]
        public StatusSale StatusSale { get; set;} 
    }
}