using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain.Entities
{
    public class SaleItemsEntity : BaseEntity
    {
        [Required]
        [ForeignKey("Fk_Sale_ItemVenda")]
        public int SaleId { get; set; }

        public SaleEntity Sale { get; set; }

        [Required]
        [ForeignKey("Fk_Product_ItemVenda")]
        public int ProductId { get; set; }

        public ProductEntity Product { get; set; }

        [Required]
        public int Amount { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal UnitPrice { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal TotalPrice { get; set; }
    }
}