using System.ComponentModel.DataAnnotations;

namespace App.Domain.Entities
{
    public class ProductEntity : BaseEntity
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Currency)]
        public decimal UnitPrice { get; set; }
    }
}