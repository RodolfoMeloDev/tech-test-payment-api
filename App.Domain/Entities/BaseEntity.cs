using System;
using System.ComponentModel.DataAnnotations;
using App.Domain.Enums;

namespace App.Domain.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public StatusRegister Status { get; set; }

        [Required]
        private DateTime? _dateCreated;
        public DateTime? DateCreated
        {
            get { return _dateCreated; }
            set { _dateCreated = ((value == null || value == DateTime.MinValue) ? DateTime.Now : value); }
        }

        private DateTime? _dateUpdated;
        public DateTime? DateUpdated
        {
            get { return _dateUpdated; }
            set { _dateUpdated = (value == DateTime.MinValue ? null : value); }
        }
        
        
    }
}