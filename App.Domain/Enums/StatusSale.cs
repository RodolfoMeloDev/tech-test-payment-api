namespace App.Domain.Enums
{
    public enum StatusSale
    {
        AWAITING_PAYMENT,
        PAYMENT_ACCEPT,
        SEND_TO_CARRIER,
        DELIVERED,
        CANCELED
    }
}