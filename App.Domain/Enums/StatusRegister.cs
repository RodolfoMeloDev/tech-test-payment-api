namespace App.Domain.Enums
{
    public enum StatusRegister
    {
        INACTIVE,
        ACTIVE
    }
}