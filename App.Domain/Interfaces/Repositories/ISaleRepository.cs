using System.Collections.Generic;
using System.Threading.Tasks;
using App.Domain.Entities;

namespace App.Domain.Interfaces.Repositories
{
    public interface ISaleRepository : IRepository<SaleEntity>
    {
        Task<IEnumerable<SaleEntity>> SelectAllCompleteAsync();

        Task<SaleEntity> SelectCompleteAsync(int id);        
    }
}