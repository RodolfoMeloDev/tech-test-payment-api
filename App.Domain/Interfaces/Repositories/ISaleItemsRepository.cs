using System.Collections.Generic;
using System.Threading.Tasks;
using App.Domain.Entities;

namespace App.Domain.Interfaces.Repositories
{
    public interface ISaleItemsRepository : IRepository<SaleItemsEntity>
    {
        Task<IEnumerable<SaleItemsEntity>> GetBySaleId(int saleId);
        Task<IEnumerable<SaleItemsEntity>> InsertListItems(IEnumerable<SaleItemsEntity> listItems);
        Task<bool> DeleteBySaleId(int saleId);
        Task<decimal> TotalSale(int saleId);
    }
}