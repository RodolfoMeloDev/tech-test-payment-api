using System.Threading.Tasks;
using App.Domain.Entities;

namespace App.Domain.Interfaces.Repositories
{
    public interface ISellerRepository : IRepository<SellerEntity>
    {
        Task<SellerEntity> ExistSellerWithCPF(string cpf);
    }
}