using System.Collections.Generic;
using System.Threading.Tasks;
using App.Domain.Dtos.Sale;

namespace App.Domain.Interfaces.Services
{
    public interface ISaleService
    {
        Task<SaleDto> GetById(int id);
        Task<SaleCompleteDto> GetCompleteById(int id);
        Task<IEnumerable<SaleDto>> GetAll();
        Task<IEnumerable<SaleCompleteDto>> GetAllComplete();
        Task<SaleCompleteDto> Insert(SaleCreateDto sale);
        Task<SaleCompleteDto> InsertBySellerCPF(SaleCreateByCPFDto sale);
        Task<SaleCompleteDto> InsertByNewSeller(SaleCreateNewSellerDto sale);
        Task<SaleUpdateDtoResult> Update(SaleUpdateDto sale);
    }
}