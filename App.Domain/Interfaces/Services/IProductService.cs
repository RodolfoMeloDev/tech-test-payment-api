using System.Collections.Generic;
using System.Threading.Tasks;
using App.Domain.Dtos.Product;

namespace App.Domain.Interfaces.Services
{
    public interface IProductService
    {
        Task<ProductDto> GetById(int id);
        Task<IEnumerable<ProductDto>> GetAll();
        Task<ProductCreateDtoResult> Insert(ProductCreateDto product);
        Task<ProductUpdateDtoResult> Update(ProductUpdateDto product);
        Task<bool> Delete(int id);
    }
}