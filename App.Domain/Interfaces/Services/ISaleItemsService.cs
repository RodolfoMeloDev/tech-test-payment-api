using System.Collections.Generic;
using System.Threading.Tasks;
using App.Domain.Dtos.SaleItems;

namespace App.Domain.Interfaces.Services
{
    public interface ISaleItemsService
    {
        Task<SaleItemsDto> GetById(int id);
        Task<IEnumerable<SaleItemsDto>> GetBySaleId(int saleId);
        Task<IEnumerable<SaleItemsDto>> GetAll();
        Task<SaleItemsCreateDtoResult> Insert(SaleItemsCreateDto saleItem);
        Task<IEnumerable<SaleItemsCreateDtoResult>> InsertListItems(int saleId, IEnumerable<SaleItemsCreateListDto> listItems);
        Task<bool> DeleteById(int id);
        Task<bool> DeleteBySaleId(int saleId);
        Task<decimal> GetTotalSale(int saleId);
    }
}