using App.Domain.Dtos.Seller;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Domain.Interfaces.Services
{
    public interface ISellerService
    {
        Task<SellerDto> GetById(int id);
        Task<SellerDto> GetByCPF(string cpf);
        Task<IEnumerable<SellerDto>> GetAll();
        Task<SellerCreateDtoResult> Insert(SellerCreateDto seller);
        Task<SellerUpdateDtoResult> Update(SellerUpdateDto seller);
        Task<bool> Delete(int id);
    }
}