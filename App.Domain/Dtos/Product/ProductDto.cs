using App.Domain.Dtos.BaseDto;

namespace App.Domain.Dtos.Product
{
    public class ProductDto : GeneralDto
    {
        public string Name { get; set; }
        public decimal UnitPrice { get; set; }
    }
}