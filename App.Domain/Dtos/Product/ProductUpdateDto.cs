using System.ComponentModel.DataAnnotations;
using App.Domain.Enums;

namespace App.Domain.Dtos.Product
{
    public class ProductUpdateDto
    {
        [Required(ErrorMessage = "O campo é obrigatório")]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        [StringLength(100, ErrorMessage = "O campo deve ter no máximo {1} caracteres.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        public decimal UnitPrice { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        public StatusRegister Status { get; set;}
    }
}