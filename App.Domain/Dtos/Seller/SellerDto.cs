using App.Domain.Dtos.BaseDto;

namespace App.Domain.Dtos.Seller
{
    public class SellerDto: GeneralDto
    {
        public string Name { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}