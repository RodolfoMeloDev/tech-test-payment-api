using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using App.Domain.Enums;

namespace App.Domain.Dtos.Seller
{
    public class SellerUpdateDto
    {
        [Required(ErrorMessage = "O campo é obrigatório")]
        public int Id { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        [StringLength(100, ErrorMessage = "O campo deve ter no máximo {1} caracteres.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        [StringLength(14, ErrorMessage = "O campo deve ter no máximo {1} caracteres.")]
        [RegularExpression("^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$", ErrorMessage ="CPF deve ser prenchido com pontos e traço. Exemplo: XXX.XXX.XXX-XX") ]
        public string Cpf { get; set; }

        [StringLength(100, ErrorMessage = "O campo deve ter no máximo {1} caracteres.")]
        [EmailAddress(ErrorMessage = "Informe um e-mail válido.")]
        public string Email { get; set; }
        
        [Phone]
        [StringLength(16, ErrorMessage = "O campo deve ter no máximo {1} caracteres.")]
        [RegularExpression("^\\(\\d{2}\\) \\d{5}-\\d{4}$", ErrorMessage ="Telefone deve ser preenchido da seguinte maneira. Exemplo: (XX) XXXXX-XXXX") ]
        public string Phone { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        public StatusRegister Status { get; set;}
    }
}