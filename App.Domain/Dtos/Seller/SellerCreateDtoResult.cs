using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using App.Domain.Dtos.BaseDto;

namespace App.Domain.Dtos.Seller
{
    public class SellerCreateDtoResult : CreateDtoResult
    {
        public string Name { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}