using System;
using System.Collections.Generic;
using App.Domain.Dtos.BaseDto;
using App.Domain.Dtos.SaleItems;
using App.Domain.Dtos.Seller;
using App.Domain.Enums;

namespace App.Domain.Dtos.Sale
{
    public class SaleCompleteDto : GeneralDto
    {
        public int SellerId { get; set; }
        public SellerDto Seller { get; set; }
        public DateTime DatePurchase{ get; set; }
        public StatusSale StatusSale { get; set; }
        public decimal TotalSale { get; set; }
        public IEnumerable<SaleItemsDto> SaleItems { get; set; }
    }
}