using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using App.Domain.Dtos.SaleItems;

namespace App.Domain.Dtos.Sale
{
    public class SaleCreateDto
    {
        [Required(ErrorMessage = "O campo é obrigatório")]
        public int SellerId { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        public DateTime DatePurchase { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        public List<SaleItemsCreateListDto> saleItems { get; set; }
    }
}