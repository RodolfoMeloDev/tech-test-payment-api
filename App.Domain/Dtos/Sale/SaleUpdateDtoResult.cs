using System;
using App.Domain.Dtos.BaseDto;
using App.Domain.Enums;

namespace App.Domain.Dtos.Sale
{
    public class SaleUpdateDtoResult : UpdateDtoResult
    {
        public int SellerId { get; set; }
        public DateTime DatePurchase{ get; set; }
        public StatusSale StatusSale { get; set; }
    }
}