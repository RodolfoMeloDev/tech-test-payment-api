using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using App.Domain.Dtos.SaleItems;

namespace App.Domain.Dtos.Sale
{
    public class SaleCreateByCPFDto
    {
        [Required(ErrorMessage = "O campo é obrigatório")]
        [StringLength(14, ErrorMessage = "O campo deve ter no máximo {1} caracteres.")]
        [RegularExpression("^\\d{3}\\.\\d{3}\\.\\d{3}-\\d{2}$", ErrorMessage ="CPF deve ser prenchido com pontos e traço. Exemplo: XXX.XXX.XXX-XX") ]
        public string Cpf { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        public DateTime DatePurchase { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        public List<SaleItemsCreateListDto> saleItems { get; set; }
    }
}