using App.Domain.Dtos.BaseDto;
using App.Domain.Enums;
using System;

namespace App.Domain.Dtos.Sale
{
    public class SaleCreateDtoResult : CreateDtoResult
    {
        public int SellerId { get; set; }
        public DateTime DatePurchase { get; set; }
        public StatusSale StatusSale { get; set; }
    }
}