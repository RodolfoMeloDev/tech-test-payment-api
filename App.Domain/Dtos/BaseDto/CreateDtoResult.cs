using System;
using App.Domain.Enums;

namespace App.Domain.Dtos.BaseDto
{
    public class CreateDtoResult
    {
        public int Id { get; set; }
        public StatusRegister Status { get; set; }

        public DateTime DateCreated { get; set; }
    }
}