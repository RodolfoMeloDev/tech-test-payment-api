using System.ComponentModel.DataAnnotations;

namespace App.Domain.Dtos.SaleItems
{
    public class SaleItemsCreateDto
    {
        [Required(ErrorMessage = "O campo é obrigatório")]
        public int SaleId { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        public int ProductId { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        public int Amount { get; set; }

        [Required(ErrorMessage = "O campo é obrigatório")]
        [DataType(DataType.Currency)]
        public decimal UnitPrice { get; set; }
    }
}