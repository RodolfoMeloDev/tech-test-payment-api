using App.Domain.Dtos.BaseDto;

namespace App.Domain.Dtos.SaleItems
{
    public class SaleItemsCreateDtoResult : CreateDtoResult
    {
        public int ProductId { get; set; }
        public int Amount { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
}