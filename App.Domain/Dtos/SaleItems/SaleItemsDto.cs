using App.Domain.Dtos.BaseDto;

namespace App.Domain.Dtos.SaleItems
{
    public class SaleItemsDto : GeneralDto
    {
        public int SaleId { get; set; }
        public int ProductId { get; set; }
        public int Amount { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
}