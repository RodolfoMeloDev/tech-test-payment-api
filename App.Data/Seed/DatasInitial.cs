using System;
using App.Data.Context;
using App.Domain.Entities;
using App.Domain.Enums;

namespace App.Data.Seed
{
    public class DatasInitial
    {
        private static void IncludeSellers(SalesContext context)
        {
            context.Sellers.Add(new SellerEntity {
                Id = 1,
                Name = "Vendedor 1",
                Cpf = "950.350.460-01",
                Email = "vendedor1@pottencial.com.br",
                Phone = "(48) 99999-9999",
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.Sellers.Add(new SellerEntity {
                Id = 2,
                Name = "Vendedor 2",
                Cpf = "124.616.920-76",
                Email = "vendedor2@pottencial.com.br",
                Phone = "(48) 99999-9999",
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now   
            });

            context.Sellers.Add(new SellerEntity {
                Id = 3,
                Name = "Vendedor 3",
                Cpf = "927.742.900-31",
                Email = "vendedor3@pottencial.com.br",
                Phone = "(48) 99999-9999",
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.Sellers.Add(new SellerEntity {
                Id = 4,
                Name = "Vendedor 4",
                Cpf = "551.999.490-04",
                Email = "vendedor4@pottencial.com.br",
                Phone = "(48) 99999-9999",
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.SaveChanges();
        }

        private static void IncludeProducts(SalesContext context)
        {
            context.Products.Add(new ProductEntity {
                Id = 1,
                Name = "Produto 1",
                UnitPrice = Convert.ToDecimal(2.58),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now                
            });

            context.Products.Add(new ProductEntity {
                Id = 2,
                Name = "Produto 1",
                UnitPrice = Convert.ToDecimal(5.8),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now                
            });

            context.Products.Add(new ProductEntity {
                Id = 3,
                Name = "Produto 1",
                UnitPrice = Convert.ToDecimal(3.99),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now                
            });

            context.Products.Add(new ProductEntity {
                Id = 4,
                Name = "Produto 4",
                UnitPrice = Convert.ToDecimal(10.50),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now                
            });

            context.SaveChanges();
        }

        private static void IncludeSales(SalesContext context)
        {
            context.Sales.Add(new SaleEntity {
                Id = 1,
                SellerId = 1,
                DatePurchase = DateTime.Now,
                StatusSale = StatusSale.AWAITING_PAYMENT,
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.Sales.Add(new SaleEntity {
                Id = 2,
                SellerId = 2,
                DatePurchase = DateTime.Now,
                StatusSale = StatusSale.AWAITING_PAYMENT,
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.Sales.Add(new SaleEntity {
                Id = 3,
                SellerId = 3,
                DatePurchase = DateTime.Now,
                StatusSale = StatusSale.AWAITING_PAYMENT,
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.Sales.Add(new SaleEntity {
                Id = 4,
                SellerId = 4,
                DatePurchase = DateTime.Now,
                StatusSale = StatusSale.AWAITING_PAYMENT,
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.SaveChanges();

            context.SaleItems.Add(new SaleItemsEntity {
                Id = 1,
                SaleId = 1,
                ProductId = 1,
                Amount = 2,
                UnitPrice = Convert.ToDecimal(3.50),
                TotalPrice = Convert.ToDecimal(7),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.SaleItems.Add(new SaleItemsEntity {
                Id = 2,
                SaleId = 1,
                ProductId = 2,
                Amount = 1,
                UnitPrice = Convert.ToDecimal(10.99),
                TotalPrice = Convert.ToDecimal(10.99),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.SaleItems.Add(new SaleItemsEntity {
                Id = 3,
                SaleId = 2,
                ProductId = 3,
                Amount = 5,
                UnitPrice = Convert.ToDecimal(2),
                TotalPrice = Convert.ToDecimal(10),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.SaleItems.Add(new SaleItemsEntity {
                Id = 4,
                SaleId = 3,
                ProductId = 4,
                Amount = 1,
                UnitPrice = Convert.ToDecimal(19.90),
                TotalPrice = Convert.ToDecimal(19.90),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.SaleItems.Add(new SaleItemsEntity {
                Id = 5,
                SaleId = 3,
                ProductId = 1,
                Amount = 8,
                UnitPrice = Convert.ToDecimal(4.50),
                TotalPrice = Convert.ToDecimal(36),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.SaleItems.Add(new SaleItemsEntity {
                Id = 6,
                SaleId = 4,
                ProductId = 1,
                Amount = 2,
                UnitPrice = Convert.ToDecimal(1.28),
                TotalPrice = Convert.ToDecimal(2.56),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.SaleItems.Add(new SaleItemsEntity {
                Id = 7,
                SaleId = 4,
                ProductId = 2,
                Amount = 8,
                UnitPrice = Convert.ToDecimal(10.67),
                TotalPrice = Convert.ToDecimal(85.36),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.SaleItems.Add(new SaleItemsEntity {
                Id = 8,
                SaleId = 4,
                ProductId = 3,
                Amount = 8,
                UnitPrice = Convert.ToDecimal(1.50),
                TotalPrice = Convert.ToDecimal(12),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.SaleItems.Add(new SaleItemsEntity {
                Id = 9,
                SaleId = 4,
                ProductId = 4,
                Amount = 8,
                UnitPrice = Convert.ToDecimal(4.33),
                TotalPrice = Convert.ToDecimal(34.64),
                Status = StatusRegister.ACTIVE,
                DateCreated = DateTime.Now
            });

            context.SaveChanges();
        }

        public static void InsertDatasInitial(SalesContext context){
            IncludeSellers(context);
            IncludeProducts(context);
            IncludeSales(context);
        }
    }
}