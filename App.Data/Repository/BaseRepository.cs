using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using App.Data.Context;
using App.Domain.Entities;
using App.Domain.Enums;
using App.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace App.Data.Repository
{
    public class BaseRepository<T> : IRepository<T> where T : BaseEntity
    
    {
        protected readonly SalesContext _context;
        private DbSet<T> _dataSet;

        public BaseRepository(SalesContext context)
        {
            _context = context;
            _dataSet = context.Set<T>();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                var result = await SelectAsync(id);

                if (result == null)
                    return false;

                _dataSet.Remove(result);
                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        public async Task<bool> ExistAsync(int id)
        {
            return await _dataSet.AnyAsync(obj => obj.Id.Equals(id));
        }

        public async Task<T> InsertAsync(T item)
        {
            try
            {
                item.DateCreated = DateTime.Now;
                item.Status = StatusRegister.ACTIVE;

                _dataSet.Add(item);
                await _context.SaveChangesAsync();

                return item;
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        public async Task<IEnumerable<T>> SelectAllAsync()
        {
            try
            {
                return await _dataSet.ToListAsync();
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        public async Task<T> SelectAsync(int id)
        {
            try
            {
                return await _dataSet.FirstOrDefaultAsync(obj => obj.Id.Equals(id));                            
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        public async Task<T> UpdateAsync(T item)
        {
            try
            {
                var result = await SelectAsync(item.Id);

                if (result == null)
                    throw new Exception("A chave de identificação do objeto não foi encontrada, não foi possível atualizar as informações.");

                if (item.Status != StatusRegister.ACTIVE && item.Status != StatusRegister.INACTIVE)
                    throw new Exception("Status do Cadastro inválido.");

                item.DateCreated = result.DateCreated;
                item.DateUpdated = DateTime.Now;

                _context.Entry(result).CurrentValues.SetValues(item);

                await _context.SaveChangesAsync();

                return item;
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }
    }
}