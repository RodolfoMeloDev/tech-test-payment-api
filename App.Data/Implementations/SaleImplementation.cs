using System.Collections.Generic;
using System.Threading.Tasks;
using App.Data.Context;
using App.Data.Repository;
using App.Domain.Entities;
using App.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace App.Data.Implementations
{
    public class SaleImplementation : BaseRepository<SaleEntity>, ISaleRepository
    {
        private DbSet<SaleEntity> _dataSet;

        public SaleImplementation(SalesContext context) : base(context)
        {
            _dataSet = _context.Set<SaleEntity>();
        }

        public async Task<IEnumerable<SaleEntity>> SelectAllCompleteAsync()
        {
            return await _dataSet.Include(s => s.Seller).ToListAsync();
        }

        public async Task<SaleEntity> SelectCompleteAsync(int id)
        {
            return await _dataSet.Include(s => s.Seller).FirstOrDefaultAsync(s => s.Id.Equals(id));
        }
    }
}