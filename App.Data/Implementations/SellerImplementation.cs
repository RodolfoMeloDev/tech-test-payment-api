using System.Threading.Tasks;
using App.Data.Context;
using App.Data.Repository;
using App.Domain.Entities;
using App.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace App.Data.Implementations
{
    public class SellerImplementation : BaseRepository<SellerEntity>, ISellerRepository
    {
        private DbSet<SellerEntity> _dataSet;

        public SellerImplementation(SalesContext context) : base(context)
        {
            _dataSet = _context.Set<SellerEntity>();
        }

        public async Task<SellerEntity> ExistSellerWithCPF(string cpf)
        {
            return await _dataSet.FirstOrDefaultAsync(obj => obj.Cpf.Equals(cpf));
        }
    }
}