using System.Collections.Generic;
using System.Threading.Tasks;
using App.Data.Context;
using App.Data.Repository;
using App.Domain.Entities;
using App.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace App.Data.Implementations
{
    public class SaleItemsImplementation : BaseRepository<SaleItemsEntity>, ISaleItemsRepository
    {
        private DbSet<SaleItemsEntity> _dataSet;

        public SaleItemsImplementation(SalesContext context) : base(context)
        {
            _dataSet = _context.Set<SaleItemsEntity>();
        }

        public async Task<IEnumerable<SaleItemsEntity>> InsertListItems(IEnumerable<SaleItemsEntity> listItems)
        {
            try
            {
                _dataSet.AddRange(listItems);
                await _context.SaveChangesAsync();

                return listItems;
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        public async Task<bool> DeleteBySaleId(int saleId)
        {
            try
            {
                var listResult = await GetBySaleId(saleId);                

                if (listResult == null || listResult.Count().Equals(0))
                    return false;

                _dataSet.RemoveRange(listResult);
                await _context.SaveChangesAsync();

                return true;
            }
            catch (Exception ex)
            {                
                throw ex;
            }
        }

        public async Task<IEnumerable<SaleItemsEntity>> GetBySaleId(int saleId)
        {
            return await _dataSet.Where(x => x.SaleId.Equals(saleId)).ToListAsync();
        }        

        public async Task<decimal> TotalSale(int saleId)
        {
            return await _dataSet.Where(x => x.SaleId.Equals(saleId)).SumAsync(v => v.TotalPrice);
        }
    }
}