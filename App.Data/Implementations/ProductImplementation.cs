using App.Data.Context;
using App.Data.Repository;
using App.Domain.Entities;
using App.Domain.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace App.Data.Implementations
{
    public class ProductImplementation : BaseRepository<ProductEntity>, IProductRepository
    {
        private DbSet<ProductEntity> _dataSet;
        
        public ProductImplementation(SalesContext context) : base(context)
        {
            _dataSet = _context.Set<ProductEntity>();
        }
    }
}